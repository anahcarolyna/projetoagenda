package br.com.itau;


import java.util.ArrayList;

public class Contato {
    private String nome;
    private String email;
    private String telefone;
    protected ArrayList<Contato> contato;

    public Contato(String nome, String email, String telefone){
        contato = new ArrayList<>();
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
    }

    public Contato(){
        contato = new ArrayList<>();
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public String getTelefone() {
        return telefone;
    }

    public String imprimirContato(){
        return "Nome: " + this.nome + "\n Email: " + this.email + "\n" + "Telefone: " + this.telefone;
    }
}
