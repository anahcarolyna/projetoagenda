package br.com.itau;

import javax.swing.*;

public class GerenciadorAgenda {
    public static void InicializarAgenda(){
        int opcaoSelecionada;
        int valorSelecionadaBotao;
        Object selecao;
        Object[] options;

        do {
            JDialog.setDefaultLookAndFeelDecorated(true);
            opcaoSelecionada = Integer.parseInt(
                    JOptionPane.showInputDialog(null, "Menu de opções: " +
                            "\n1- Incluir novo contato" +
                            "\n2- Remover contato" +
                            "\n3- Listar contato" +
                            "\n4- Sair da agenda" +
                            "\nEscolha uma opção."));

            switch (opcaoSelecionada){
                case 1:
                 int qtdeRegistros = Integer.parseInt(JOptionPane.showInputDialog("Quantos contatos deseja incluir?"));

                 for(int contador =0; contador < qtdeRegistros;contador++) {
                     Contato contato = new Contato(JOptionPane.showInputDialog("Nome:"),
                             JOptionPane.showInputDialog("Email:"),
                             JOptionPane.showInputDialog("Telefone"));

                     Agenda.adicionarContato(contato);
                     JOptionPane.showMessageDialog(null, "Contato incluído com sucesso!");
                 }
                break;
                case 2:
                    options = new Object[] {"Email", "Número do tefone"};
                    selecao = JOptionPane.showInputDialog(null, "Favor selecione o filtro para exclusão:",
                            "Remover contato", JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);

                    if(selecao == "Email"){
                        Agenda.removerContatoEmail(JOptionPane.showInputDialog("Email:"));
                        JOptionPane.showMessageDialog(null, "Contato removido com sucesso!");
                    }
                    else
                    {
                        Agenda.removerContatoNumero(JOptionPane.showInputDialog("Número:"));
                        JOptionPane.showMessageDialog(null, "Contato removido com sucesso!");
                    }
                    break;

                case 3:
                    options = new Object[] {"Todos", "Email", "Número do tefone"};
                    selecao = JOptionPane.showInputDialog(null, "Favor selecionar o filtro para listagem:",
                            "Listar contato", JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);

                     if(selecao.equals("Email")) {
                         JOptionPane.showMessageDialog(null, Agenda.buscarPorEmail(JOptionPane.showInputDialog("Email:")));
                     }
                    else if(selecao.equals("Número do tefone")){
                        JOptionPane.showMessageDialog(null, Agenda.buscarPorTelefone(JOptionPane.showInputDialog("Telefone:")));
                    }
                    else{
                            JOptionPane.showMessageDialog(null, Agenda.buscarTodos());
                        }
                    break;

                case 4:
                    valorSelecionadaBotao = JOptionPane.showConfirmDialog(null,"Deseja sair do sistema?","Sair", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if(valorSelecionadaBotao == 0){
                        System.exit(0);
                    }
                    break;

                default:
                    JOptionPane.showMessageDialog(null, "Opção inválida! Escolha uma opção válida no menu");
            }
        }
        while(opcaoSelecionada >= 1 && opcaoSelecionada <= 4);

        System.exit(0);
    }
}
