package br.com.itau;

import com.sun.management.UnixOperatingSystemMXBean;

import java.util.ArrayList;

public class Agenda {

    private static ArrayList<Contato> agenda = new ArrayList<Contato>();

    public ArrayList<Contato> getContato() {
        return agenda;
    }

    public static void adicionarContato(Contato contato)
    {
        agenda.add(contato);
    }

    public static void removerContatoEmail(String email)
    {
        for (int i=0;i < agenda.size();i++)
        {
            if(agenda.get(i).getEmail().equalsIgnoreCase(email)){
                agenda.remove(i);
            }
        }
    }

    public static void removerContatoNumero(String telefone)
    {
        for (int i=0;i < agenda.size();i++)
        {
            if(agenda.get(i).getTelefone().equalsIgnoreCase(telefone)){
                agenda.remove(i);
            }
        }
    }

    public static String buscarPorEmail(String email) {
        for (Contato contato : agenda) {
            if (contato.getEmail().equalsIgnoreCase(email)) {
                return contato.imprimirContato() + "\n";
            }
        }

        return "Nenhum contato cadastrado";
    }

    public static String buscarPorTelefone(String telefone){
        for (Contato contato : agenda) {
            if (contato.getTelefone().equalsIgnoreCase(telefone)) {
                return contato.imprimirContato() + "\n";
            }
        }
        return "Nenhum contato cadastrado";
    }


    public static String buscarTodos()
    {
        String imprimirContato = "";
        for(Contato contato : agenda) {
            imprimirContato += contato.imprimirContato() + "\n\n";
        }
        return imprimirContato;
    }
}
